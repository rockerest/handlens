/* global describe it */

import trim from "../js/trim.js";

describe( "token trimming function - trim.js", function TrimSuite(){
	var allTokens = [ "alpha!", "beta.", "st!ff", "'finger", "guns'", "title:search", "!@#$%^&*()_+=-0987654321[]\\;',./{}|:\"<>?" ];
	var trimmedTokens = [ "alpha", "beta", "stff", "finger", "guns", "title:search", "@#$%^-0987654321':" ];

	it( "should remove punctuation from tokens", () => {
		trim( allTokens ).should.deep.equal( trimmedTokens );
	} );

	it( "should not remove colons from tokens", () => {
		// colons are used to designate a field search
		trim( allTokens )[ 5 ].should.deep.equal( trimmedTokens[ 5 ] );
	} );

	it( "should not remove more widely used puncuation from tokens", () => {
		trim( allTokens )[ 6 ].should.deep.equal( trimmedTokens[ 6 ] );
	} );

	it( "should remove apostrophes from tokens if they are at the beginning or the end", () => {
		trim( allTokens )[ 3 ].should.deep.equal( trimmedTokens[ 3 ] );
		trim( allTokens )[ 4 ].should.deep.equal( trimmedTokens[ 4 ] );
	} );
} );
