/* global describe it */

import settings from "../js/settings.js";
import stopwords from "../js/stopwords.js";

describe( "stopwords removal function - stopwords.js", function StopwordsSuite(){
	var allWords = settings.get().stopwords.list.en;
	var source = [
		"the", "cat", "in", "the", "hat", "by", "doctor", "seuss"
	];
	var clean = [
		"cat", "hat", "doctor", "seuss"
	];

	it( "should remove stopwords from lists of tokens and leave non-stopwords in place", () => {
		stopwords( source, allWords ).should.deep.equal( clean );
	} );
} );
