function fallback( thing, or ){
	return thing || or;
}

function recurseThroughToken( base, token ){
	var leaf = base;

	for( let i = 0; i < token.length; i++ ){
		if( leaf[ token[ i ] ] ){
			leaf = leaf[ token[ i ] ];
		}
		else{
			leaf = null;
			break;
		}
	}

	return leaf;
}

function getFieldLeafForToken( fieldIndex, token ){
	var leaf = null;

	if( token ){
		leaf = recurseThroughToken( fieldIndex, token );
	}

	return fallback( leaf, { "documents": {}, "tokenFrequency": 0 } );
}

function getDocumentsForTokenInField( token, field ){
	var leaf = getFieldLeafForToken( field, token );

	return fallback( leaf.documents, {} );
}

function searchField( tokens, field, bool ){
	var handlers = {
		OR( tkns, fld ){
			var docs = new Set();

			tkns.forEach( ( token ) => {
				Object
					.keys( getDocumentsForTokenInField( token, fld ) )
					.forEach( ( ref ) => {
						docs.add( ref );
					} );
			} );

			return Array.from( docs );
		},
		AND( tkns, fld ){
			var docs = new Set();
			var found = null;

			tkns.forEach( ( token ) => {
				if( !found ){
					found = new Set();
					Object
						.keys( getDocumentsForTokenInField( token, fld ) )
						.forEach( ( ref ) => {
							found.add( ref );
						} );
				}
			} );

			if( found ){
				found.forEach( ( ref ) => docs.add( ref ) );
			}

			return Array.from( docs );
		}
	};

	return handlers[ bool ] ? handlers[ bool ]( tokens, field ) : [];
}

function mergeAnyAnd( prior, neue ){
	var mergedAnd;

	function getMerged( p, n ){
		var newStorage = new Set();

		for( let doc of n ){
			if( p.has( doc ) ){
				newStorage.add( doc );
			}
		}

		return newStorage;
	}

	if( !prior ){
		mergedAnd = new Set( neue );
	}
	else{
		mergedAnd = getMerged( prior, neue );
	}

	return mergedAnd;
}

function fullAnyAndMatches( index, qry ){
	var tokens = {};
	var final = null;

	qry.fields[ "*" ].forEach( ( token ) => {
		let matches = new Set();

		index.fields.forEach( ( field ) => {
			searchField( [ token ], index.index[ field ], "OR" )
				.forEach( ( doc ) => {
					matches.add( doc );
				} );
		} );

		tokens[ token ] = matches;
	} );

	Object
		.values( tokens )
		.forEach( ( matches ) => {
			final = mergeAnyAnd( final, matches );
		} );

	return fallback( final, new Set() );
}

function processAnd( index, q ){
	var matches = new Set();
	var final = new Set();
	var any = fullAnyAndMatches( index, q );

	index.fields.forEach( ( field ) => {
		let tokens = fallback( q.fields[ field ], [] );
		let f = index.index[ field ];

		if( tokens.length ){
			searchField( tokens, f, "AND" )
				.forEach( ( doc ) => {
					matches.add( doc );
				} );
		}
	} );

	for( let v of matches ){
		final.add( v );
	}

	for( let v of any ){
		final.add( v );
	}

	return Array.from( final );
}

function processQuery( index, qry ){
	var handlers = {
		OR( idx, q ){
			var matches = new Set();

			idx.fields.forEach( ( field ) => {
				let tokens = fallback( q.fields[ field ], q.fields[ "*" ] );

				if( tokens.length ){
					searchField( tokens, idx.index[ field ], "OR" )
						.forEach( ( doc ) => {
							matches.add( doc );
						} );
				}
			} );

			return Array.from( fallback( matches, [] ) );
		},
		AND( idx, q ){
			return processAnd( index, q );
		}
	};

	return handlers[ qry.bool ] ? handlers[ qry.bool ]( index, qry ) : [];
}

function query( index, queries ){
	var uniqed  = new Set();

	queries
		.map( ( q ) => processQuery( index, q ) )
		.reduce( ( all, matches ) => all.concat( matches ), [] )
		.forEach( ( ref ) => uniqed.add( ref ) );

	return Array
		.from( uniqed )
		.map( ( docRef ) => ( {
			"ref": docRef
		} ) ); // Mapping to an object here so that in the future it can have things like full documents and match scoring
}

export default query;
