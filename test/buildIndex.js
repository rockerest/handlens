/* global describe it sinon */

import tokenize from "../js/tokenize.js";
import trim from "../js/trim.js";

import buildIndex from "../js/buildIndex.js";

function testBareInvertedIndexRoot( name, idx ){
	idx.should.have.any.keys( [ name ] );
	idx[ name ].should.have.all.keys( [ "documents", "documentFrequency" ] );
	idx[ name ].documents.should.deep.equal( {} );
	idx[ name ].documentFrequency.should.deep.equal( 0 );
}

function build( override = {} ){
	var cloneableIndex = {
		"fields": [],
		"documents": [],
		"pipeline": [],
		"ref": "id"
	};

	return buildIndex.full( Object.assign( {}, cloneableIndex, override ) );
}

describe( "buildIndex function - buildIndex.js", function BuildIndexSuite(){
	it( "should expose the expected API", () => {
		buildIndex.should.have.all.keys( [
			"full",
			"addDocument",
			"addField"
		] );
	} );

	describe( "#full", function FullSuite(){
		it( "should return an empty index if the source index has no fields", () => {
			buildIndex.full( { "fields": undefined } ).should.deep.equal( {} );
			buildIndex.full( { "fields": null } ).should.deep.equal( {} );
			buildIndex.full( { "fields": 1 } ).should.deep.equal( {} );
			buildIndex.full( { "fields": "one" } ).should.deep.equal( {} );
			buildIndex.full( { "fields": /regex/ } ).should.deep.equal( {} );
			buildIndex.full( { "fields": Symbol() } ).should.deep.equal( {} );
			buildIndex.full( { "fields": new Set() } ).should.deep.equal( {} );
			buildIndex.full( { "fields": new Map() } ).should.deep.equal( {} );
		} );

		it( "should return an empty index if the source index has no documents", () => {
			buildIndex.full( { "documents": undefined } ).should.deep.equal( {} );
			buildIndex.full( { "documents": null } ).should.deep.equal( {} );
			buildIndex.full( { "documents": 1 } ).should.deep.equal( {} );
			buildIndex.full( { "documents": "one" } ).should.deep.equal( {} );
			buildIndex.full( { "documents": /regex/ } ).should.deep.equal( {} );
			buildIndex.full( { "documents": Symbol() } ).should.deep.equal( {} );
			buildIndex.full( { "documents": new Set() } ).should.deep.equal( {} );
			buildIndex.full( { "documents": new Map() } ).should.deep.equal( {} );
		} );

		it( "should return an empty index if the source index has no pipeline stages", () => {
			buildIndex.full( { "pipeline": undefined } ).should.deep.equal( {} );
			buildIndex.full( { "pipeline": null } ).should.deep.equal( {} );
			buildIndex.full( { "pipeline": 1 } ).should.deep.equal( {} );
			buildIndex.full( { "pipeline": "one" } ).should.deep.equal( {} );
			buildIndex.full( { "pipeline": /regex/ } ).should.deep.equal( {} );
			buildIndex.full( { "pipeline": Symbol() } ).should.deep.equal( {} );
			buildIndex.full( { "pipeline": new Set() } ).should.deep.equal( {} );
			buildIndex.full( { "pipeline": new Map() } ).should.deep.equal( {} );
		} );

		it( "should return an empty index if the source index has no reference key", () => {
			buildIndex.full( { "ref": undefined } ).should.deep.equal( {} );
			buildIndex.full( { "ref": null } ).should.deep.equal( {} );
			buildIndex.full( { "ref": 1 } ).should.deep.equal( {} );
			buildIndex.full( { "ref": [ "one" ] } ).should.deep.equal( {} );
			buildIndex.full( { "ref": /regex/ } ).should.deep.equal( {} );
			buildIndex.full( { "ref": Symbol() } ).should.deep.equal( {} );
			buildIndex.full( { "ref": new Set() } ).should.deep.equal( {} );
			buildIndex.full( { "ref": new Map() } ).should.deep.equal( {} );
		} );

		it( "should return an empty index if the source index is not an object", () => {
			buildIndex.full( undefined ).should.deep.equal( {} );
			buildIndex.full( null ).should.deep.equal( {} );
			buildIndex.full( 1 ).should.deep.equal( {} );
			buildIndex.full( "one" ).should.deep.equal( {} );
			buildIndex.full( [ "one" ] ).should.deep.equal( {} );
			buildIndex.full( /regex/ ).should.deep.equal( {} );
			buildIndex.full( Symbol() ).should.deep.equal( {} );
			buildIndex.full( new Set() ).should.deep.equal( {} );
			buildIndex.full( new Map() ).should.deep.equal( {} );
		} );

		describe( "given that at least all the expected values are present with the right types", function OstensiblyCorrectSuite(){
			describe( "Fields", function FieldsSuite(){
				it( "should return an empty index if there are no fields", () => {
					build().should.deep.equal( {} );
				} );

				it( "should create a root inverted index for each field", () => {
					let fieldName = "testField";
					let idx = build( { "fields": [ fieldName ] } );

					idx.should.have.all.keys( [ fieldName ] );
					testBareInvertedIndexRoot( fieldName, idx );
				} );
			} );

			describe( "Documents", function DocumentsSuite(){
				it( "should return bare inverted index roots if there are no documents", () => {
					let fieldName = "testField";
					let idx = build( { "fields": [ fieldName + 1, fieldName + 2 ] } );

					idx.should.have.all.keys( [ fieldName + 1, fieldName + 2 ] );
					testBareInvertedIndexRoot( fieldName + 1, idx );
					testBareInvertedIndexRoot( fieldName + 2, idx );
				} );

				it( "should leave the inverted index roots as empty leaves if there are no tokens", () => {
					let ppl1 = sinon.stub().returns( [] );
					let emptyLeaf = { "documents": {}, "documentFrequency": 0 };

					let idx = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": "alpha1",
								"beta": "beta1"
							},
							{
								"id": 2,
								"alpha": "alpha2",
								"beta": "beta2"
							}
						],
						"pipeline": [
							ppl1
						],
						"ref": "id"
					} );

					idx.should.have.all.keys( [ "alpha", "beta" ] );
					idx.alpha.should.deep.equal( emptyLeaf );
					idx.beta.should.deep.equal( emptyLeaf );
				} );

				it( "should assign document references at the correct leaves", () => {
					let idx = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": "pen dog cat",
								"beta": "parrot"
							},
							{
								"id": 2,
								"alpha": "penguin dog cat",
								"beta": "ferret"
							}
						],
						"pipeline": [
							tokenize,
							trim
						],
						"ref": "id"
					} );

					// all of the token-based leaves
					idx.should.have.nested.property( "alpha.p.e.n.g.u.i.n" );
					idx.should.have.nested.property( "alpha.d.o.g" );
					idx.should.have.nested.property( "alpha.c.a.t" );
					idx.should.have.nested.property( "beta.p.a.r.r.o.t" );
					idx.should.have.nested.property( "beta.f.e.r.r.e.t" );

					// all of the document references
					idx.should.have.nested.property( "alpha.p.e.n.documents.1" );
					idx.should.have.nested.property( "alpha.p.e.n.g.u.i.n.documents.2" );
					idx.should.have.nested.property( "alpha.d.o.g.documents.1" );
					idx.should.have.nested.property( "alpha.d.o.g.documents.2" );
					idx.should.have.nested.property( "alpha.c.a.t.documents.1" );
					idx.should.have.nested.property( "alpha.c.a.t.documents.2" );
					idx.should.have.nested.property( "beta.p.a.r.r.o.t.documents.1" );
					idx.should.have.nested.property( "beta.f.e.r.r.e.t.documents.2" );
				} );

				it( "should calculate the proper document frequencies based on the tokens and documents", () => {
					let idx = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": "pen dog",
								"beta": "parrot"
							},
							{
								"id": 2,
								"alpha": "pen cat",
								"beta": "ferret"
							}
						],
						"pipeline": [
							tokenize,
							trim
						],
						"ref": "id"
					} );

					idx.should.have.nested.property( "alpha.p.e.n.documentFrequency", 2 );
					idx.should.have.nested.property( "alpha.d.o.g.documentFrequency", 1 );
					idx.should.have.nested.property( "alpha.c.a.t.documentFrequency", 1 );
					idx.should.have.nested.property( "beta.p.a.r.r.o.t.documentFrequency", 1 );
					idx.should.have.nested.property( "beta.f.e.r.r.e.t.documentFrequency", 1 );
				} );
			} );

			describe( "Pipeline", function PipelineSuite(){
				it( "should return the raw field values if there are no pipeline steps", () => {
					let idx = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": "dog dog",
								"beta": "cat cat"
							},
							{
								"id": 2,
								"alpha": "cat cat",
								"beta": "dog dog"
							}
						],
						"ref": "id"
					} );

					/* Alpha Dog */
					idx.should.have.nested.property( "alpha.d.o.g" );
					// `.nested` fails on property named " "
					idx.alpha.d.o.g[ " " ].should.have.nested.property( "d.o.g" );

					/* Beta Dog */
					idx.should.have.nested.property( "beta.d.o.g" );
					// `.nested` fails on property named " "
					idx.beta.d.o.g[ " " ].should.have.nested.property( "d.o.g" );

					/* Alpha Cat */
					idx.should.have.nested.property( "alpha.c.a.t" );
					// `.nested` fails on property named " "
					idx.alpha.c.a.t[ " " ].should.have.nested.property( "c.a.t" );

					/* Beta Cat */
					idx.should.have.nested.property( "beta.c.a.t" );
					// `.nested` fails on property named " "
					idx.beta.c.a.t[ " " ].should.have.nested.property( "c.a.t" );
				} );

				it( "should process the field values through each pipeline step if there are steps", () => {
					let ppl1 = sinon.stub().returns( "2" );
					let ppl2 = sinon.stub().returns( "3" );
					let ppl3 = sinon.stub().returns( "final" );

					let a1 = "dog dog";
					let b1 = "cat cat";
					let a2 = "cat cat";
					let b2 = "dog dog";

					let idx = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": a1,
								"beta": b1
							},
							{
								"id": 2,
								"alpha": a2,
								"beta": b2
							}
						],
						"pipeline": [
							ppl1,
							ppl2,
							ppl3
						],
						"ref": "id"
					} );

					/* all pipeline steps should be called the appropriate number of times */
					ppl1.callCount.should.equal( 4 ); // once for each field (2), for each document (2)
					ppl2.callCount.should.equal( 4 );
					ppl3.callCount.should.equal( 4 );

					/* all pipeline steps should be called with the appropriate previous value */
					ppl1.getCall( 0 ).args[ 0 ].should.equal( a1 ); // document 1, "alpha"; iterates by document, then by field
					ppl1.getCall( 1 ).args[ 0 ].should.equal( b1 ); // document 1, "beta"
					ppl1.getCall( 2 ).args[ 0 ].should.equal( a2 ); // document 2, "alpha"
					ppl1.getCall( 3 ).args[ 0 ].should.equal( b2 ); // document 2, "beta"

					ppl2.getCall( 0 ).args[ 0 ].should.equal( "2" ); // predefined responses from our fake pipeline
					ppl2.getCall( 1 ).args[ 0 ].should.equal( "2" );
					ppl2.getCall( 2 ).args[ 0 ].should.equal( "2" );
					ppl2.getCall( 3 ).args[ 0 ].should.equal( "2" );

					ppl3.getCall( 0 ).args[ 0 ].should.equal( "3" );
					ppl3.getCall( 1 ).args[ 0 ].should.equal( "3" );
					ppl3.getCall( 2 ).args[ 0 ].should.equal( "3" );
					ppl3.getCall( 3 ).args[ 0 ].should.equal( "3" );

					/* The results of the pipeline should be used to create the inverted index */
					idx.should.have.nested.property( "alpha.f.i.n.a.l" ); // All of the pipelines result in the string "final"
					idx.should.have.nested.property( "beta.f.i.n.a.l" );
				} );
			} );

			describe( "Token Frequency", function TokenFrequencySuite(){
				it( "should not generate frequencies if there are no tokens", () => {
					let ppl1 = sinon.stub().returns( [] );
					let emptyLeaf = { "documents": {}, "documentFrequency": 0 };

					let idx = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": "alpha1",
								"beta": "beta1"
							},
							{
								"id": 2,
								"alpha": "alpha2",
								"beta": "beta2"
							}
						],
						"pipeline": [
							ppl1
						],
						"ref": "id"
					} );

					idx.should.have.all.keys( [ "alpha", "beta" ] );
					idx.alpha.should.deep.equal( emptyLeaf );
					idx.beta.should.deep.equal( emptyLeaf );
				} );

				it( "should calculate the token frequencies properly for the documents provided", () => {
					let idx = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": "dog dog",
								"beta": "cat cat"
							},
							{
								"id": 2,
								"alpha": "cat cat",
								"beta": "dog dog"
							}
						],
						"pipeline": [
							tokenize,
							trim
						],
						"ref": "id"
					} );

					let idx2 = build( {
						"fields": [
							"alpha",
							"beta"
						],
						"documents": [
							{
								"id": 1,
								"alpha": "cat dog",
								"beta": "cat cat"
							},
							{
								"id": 2,
								"alpha": "cat cat",
								"beta": "dog dog"
							}
						],
						"pipeline": [
							tokenize,
							trim
						],
						"ref": "id"
					} );

					idx.should.have.nested.property( "alpha.d.o.g.documents.1.tokenFrequency", Math.sqrt( 2 ) );
					idx.should.have.nested.property( "beta.d.o.g.documents.2.tokenFrequency", Math.sqrt( 2 ) );
					idx.should.have.nested.property( "alpha.c.a.t.documents.2.tokenFrequency", Math.sqrt( 2 ) );
					idx.should.have.nested.property( "beta.c.a.t.documents.1.tokenFrequency", Math.sqrt( 2 ) );

					idx2.should.have.nested.property( "alpha.d.o.g.documents.1.tokenFrequency", Math.sqrt( 1 ) );
					idx2.should.have.nested.property( "beta.d.o.g.documents.2.tokenFrequency", Math.sqrt( 2 ) );
					idx2.should.have.nested.property( "alpha.c.a.t.documents.2.tokenFrequency", Math.sqrt( 2 ) );
					idx2.should.have.nested.property( "alpha.c.a.t.documents.1.tokenFrequency", Math.sqrt( 1 ) );
					idx2.should.have.nested.property( "beta.c.a.t.documents.1.tokenFrequency", Math.sqrt( 2 ) );
				} );
			} );
		} );
	} );

	describe( "#addDocument", function AddDocumentSuite(){
		it( "should return an empty index if a falsey index is provided", () => {
			buildIndex.addDocument( undefined ).should.deep.equal( {} );
			buildIndex.addDocument( null ).should.deep.equal( {} );
			buildIndex.addDocument( 0 ).should.deep.equal( {} );
			buildIndex.addDocument( false ).should.deep.equal( {} );
		} );

		it( "should return the unmodified index if there are no fields", () => {
			buildIndex.addDocument( 1 ).should.equal( 1 );
			buildIndex.addDocument( true ).should.equal( true );
			buildIndex.addDocument( "one" ).should.equal( "one" );
		} );

		it( "should index the document properly (no tokenization) if there are fields", () => {
			let fields = [ "alpha" ];
			let idx = buildIndex.addField( {}, "alpha" );
			let testDoc = {
				"id": 1,
				"alpha": "test string"
			};

			let newIdx = buildIndex.addDocument( idx, testDoc, fields );

			newIdx.should.have.nested.property( "alpha.t.e.s.t" );
			newIdx.alpha.t.e.s.t[ " " ].should.have.deep.nested.property( "s.t.r.i.n.g", {
				"documents": {
					"1": {
						"tokenFrequency": 1
					}
				},
				"documentFrequency": 1
			} );
		} );

		it( "should process the tokens through the pipeline if one is provided", () => {
			let fields = [ "alpha" ];
			let pipeline = [
				sinon.stub().returns( "two" ),
				sinon.stub().returns( "final" )
			];
			let idx = buildIndex.addField( {}, "alpha" );
			let testDoc = {
				"id": 1,
				"alpha": "test string"
			};

			let newIdx = buildIndex.addDocument( idx, testDoc, fields, pipeline );

			pipeline[ 0 ].should.have.been.calledOnce;
			pipeline[ 1 ].should.have.been.calledOnce;

			pipeline[ 0 ].getCall( 0 ).should.have.been.calledWithExactly( "test string" );
			pipeline[ 1 ].getCall( 0 ).should.have.been.calledWithExactly( "two" );

			newIdx.should.have.deep.nested.property( "alpha.f.i.n.a.l", {
				"documents": {
					"1": {
						"tokenFrequency": 1
					}
				},
				"documentFrequency": 1
			} );
		} );
	} );

	describe( "#addField", function AddFieldSuite(){
		it( "should return the original index if the field already exists", () => {
			let updatedIndex = buildIndex.addField( { "alpha": { "documents": {}, "documentFrequency": 128 } }, "alpha" );

			updatedIndex.should.deep.equal( {
				"alpha": {
					"documents": {},
					"documentFrequency": 128
				}
			} );
		} );

		it( "should add the field to the index as an empty leaf node if the field doesn't exist", () => {
			let updatedIndex = buildIndex.addField( {}, "alpha" );

			updatedIndex.should.deep.equal( {
				"alpha": {
					"documents": {},
					"documentFrequency": 0
				}
			} );
		} );
	} );
} );
