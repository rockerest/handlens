function trie( token, root ){
	var index = 0;
	var key;

	while( index <= token.length - 1 ){
		key = token[ index ];

		if( !( key in root ) ){
			root[ key ] = {
				"documents": {},
				"documentFrequency": 0
			};
		}

		root = root[ key ];
		index++;
	}

	return root;
}

export default trie;
