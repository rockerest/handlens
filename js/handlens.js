// Much of the code for handlens was inspired by these projects:
//		http://elasticlunr.com/

import index from "./index.js";

function handlens( setup ){
	var idx = index();

	if( setup && typeof setup == "function" ){
		setup.call( idx, idx );
	}

	idx.rebuild();

	return idx;
}

export default handlens;
