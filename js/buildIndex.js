import trie from "./trie.js";

function usableAnd( item, and ){
	return item && and;
}

function usableArray( item ){
	return usableAnd( item, Array.isArray( item ) );
}

function usableString( item ){
	return usableAnd( item, ( typeof item == "string" || item instanceof String ) );
}

function usableNumber( item ){
	return usableAnd( item, ( typeof item == "number" || item instanceof Number ) && !Number.isNaN( item ) );
}

function usableStringOrNumber( item ){
	return usableString( item ) || usableNumber( item );
}

function usableObject( item ){
	return usableAnd( item, typeof item == "object" && item !== null );
}

function usableIndex( index ){
	var usableIdx = usableObject( index );
	var usableArrays;

	if( usableIdx ){
		usableArrays = [ index.fields, index.documents, index.pipeline ].reduce(
			( usable, item ) => usable && usableArray( item ), // each item should be usable
			true // start value of true
		);

		usableIdx = usableArrays && usableString( index.ref );
	}

	return usableIdx;
}

function getProcessableTokens( existingTokens ){
	var tokens = [];

	if( usableArray( existingTokens ) ){
		tokens = existingTokens;
	}
	else if( usableStringOrNumber( existingTokens ) ){
		tokens = [ String( existingTokens ) ];
	}

	return tokens;
}

function buildIndex( index ){
	var idx = {};

	if( usableIndex( index ) ){
		index.fields.forEach( ( field ) => {
			idx = addField( idx, field );
		} );

		index.documents.forEach( ( doc ) => {
			idx = addDocument( idx, doc, index.fields, index.pipeline, index.ref );
		} );
	}

	return idx;
}

function addDocument( index, doc, fields = [], pipeline = [], ref = "id" ){
	index = index || {};

	fields.forEach( ( field ) => {
		let tokens = pipeline.reduce(
			( accumulator, pipelineStep ) => pipelineStep( accumulator ),
			doc[ field ]
		);
		let docRef = doc[ ref ];
		let tokenCount = {};

		getProcessableTokens( tokens )
			.forEach( ( token ) => {
				if( token in tokenCount ){
					tokenCount[ token ] += 1;
				}
				else{
					tokenCount[ token ] = 1;
				}
			} );

		for( let token in tokenCount ){
			let root = trie( token, index[ field ] );

			if( !root.documents[ docRef ] ){
				root.documentFrequency += 1;
			}

			root.documents[ docRef ] = {
				"tokenFrequency": Math.sqrt( tokenCount[ token ] )
			};
		}
	} );

	return index;
}

function addField( index, field ){
	if( !index[ field ] ){
		index[ field ] = {
			"documents": {},
			"documentFrequency": 0
		};
	}

	return index;
}

export default {
	"full": buildIndex,
	addDocument,
	addField
};
