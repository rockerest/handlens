import settings from "./settings.js";

import tokenize from "./tokenize.js";
import trim from "./trim.js";
import stopwords from "./stopwords.js";

import buildIndex from "./buildIndex.js";
import querybuilder from "./querybuilder.js";
import query from "./query.js";

export default function index(){
	var idx = {
		"settings": settings.get(),
		"fields": [],
		"ref": "id",
		"documents": [],
		"pipeline": [
			tokenize,
			trim,
			( tokens ) => stopwords( tokens, idx.settings.stopwords.list[ idx.settings.stopwords.lang ] )
		],
		"index": {},

		addDocument( document ){
			this.index = buildIndex.addDocument( this.index, document, this.fields, this.pipeline, this.ref );

			if( this.settings.documents.retainAfterIndex ){
				this.documents.push( document );
			}
		},
		addField( field ){
			this.fields.push( field );
			this.index = buildIndex.addField( this.index, field );

			this.rebuild();
		},
		rebuild(){
			this.index = buildIndex.full( this );

			if( !this.settings.documents.retainAfterIndex ){
				this.documents = [];
			}
		},
		search( input ){
			return this.query( querybuilder( input, this ) );
		},
		query( queries ){
			return query( this, queries );
		}
	};

	return idx;
}
