import tokenize from "./tokenize.js";

function processTokens( queries, fields ){
	return queries.map( ( query ) => {
		let newQuery = Object.assign( {}, query );
		let fieldTokens = { "*": [] };
		let tokens = query.tokens;

		tokens.forEach( ( token ) => {
			let field = token.split( ":", 1 );

			if( fields.includes( field[ 0 ] ) ){
				fieldTokens[ field ] = fieldTokens[ field ] || [];
				fieldTokens[ field ].push( token.replace( `${field}:`, "" ) );
			}
			else{
				fieldTokens[ "*" ].push( token );
			}
		} );

		newQuery.fields = fieldTokens;
		delete newQuery.tokens;

		return newQuery;
	} );
}

function makeQueries( clusters, pipeline ){
	var blankQuery = {
		"bool": "OR",
		"tokens": null,
		"fields": null
	};
	var queries = clusters.map( ( cluster ) => {
		let q = Object.assign( {}, blankQuery );

		if( cluster.indexOf( " AND " ) > -1 ){
			q.bool = "AND";
		}

		q.tokens = pipeline.reduce(
			( accumulator, pipelineStep ) => pipelineStep( accumulator ),
			cluster
		);

		return q;
	} );

	return queries.filter( ( q ) => q.tokens.length );
}

function joinLogicalAnd( tokens ){
	var clusters = [ [] ];
	var lastAnd = false;
	var twoAgoAnd = false;

	function getLastToken( c ){
		return c[ c.length - 1 ].pop() || "";
	}

	function getClusterToAppend( c, taa ){
		var cluster = [];

		if( taa ){
			c.push( cluster );
			taa = false;
		}
		else{
			cluster = c[ c.length - 1 ];
		}

		return {
			"twoAgoAnd": taa,
			cluster
		};
	}

	tokens.forEach( ( token ) => {
		if( token == "AND" || lastAnd ){
			let lastToken = getLastToken( clusters );

			clusters.push( [ lastToken + " " + token ] );

			lastAnd = token == "AND";
			twoAgoAnd = !lastAnd;
		}
		else{
			let cluster;

			( { cluster, twoAgoAnd } = getClusterToAppend( clusters, twoAgoAnd ) );

			cluster.push( token );
		}
	} );

	return clusters
		.filter( ( cluster ) => cluster.length )
		.map( ( cluster ) => cluster.join( " " ) );
}

function stripLogicalOr( tokens ){
	return tokens.filter( ( t ) => t != "OR" );
}

function stripBookendLogicalAnd( input ){
	var bookends = /(?:^(?:\s*(?:AND)?\s*)*|(?:\s*(?:AND)?\s*)*$)/gm;

	return input.replace( bookends, "" );
}

function ensureAString( input ){
	var val = input;

	if( typeof input != "string" && !( input instanceof String ) ){
		val = "";
	}

	return val;
}

function querybuilder( input, index ){
	return processTokens(
		makeQueries(
			joinLogicalAnd(
				stripLogicalOr(
					tokenize(
						stripBookendLogicalAnd(
							ensureAString( input )
						),
						{ "lowercase": false }
					)
				)
			),
			index.pipeline
		),
		index.fields
	);
}

export default querybuilder;
