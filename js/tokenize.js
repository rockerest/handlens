import settings from "./settings.js";

function tokenize( inputString, config = {} ){
	var tokens = [];

	inputString = String( inputString || "" );

	config = Object.assign( {}, settings.get().tokenize, config );

	if( config.lowercase ){
		inputString = inputString.toLowerCase();
	}

	tokens = inputString
		.trim()
		.split( config.separator );

	return tokens;
}

export default tokenize;
