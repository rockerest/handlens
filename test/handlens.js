/* global sinon describe it */

import handlens from "../js/handlens.js";

describe( "handlens main entry function - handlens.js", function HandlensMainSuite(){
	it( "should call the passed in configuration function", () => {
		let spy = sinon.spy();

		handlens( spy );

		spy.should.have.been.calledOnce;
	} );

	it( "should succeed without modification if the passed in value is not a function", () => {
		handlens( null ).should.be.ok;
		handlens( "hello" ).should.be.ok;
		handlens( 10 ).should.be.ok;
	} );
} );
