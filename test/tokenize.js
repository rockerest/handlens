/* global describe it */

import tokenize from "../js/tokenize.js";

describe( "string tokenizer function - tokenize.js", function TokenizeSuite(){
	it( "should split on spaces by default", () => {
		tokenize( "this is a list of tokens" ).should.deep.equal( [ "this", "is", "a", "list", "of", "tokens" ] );
	} );

	it( "should lowercase by default", () => {
		tokenize( "THIS is a LIST of TOKENS" ).should.deep.equal( [ "this", "is", "a", "list", "of", "tokens" ] );
	} );

	it( "should use the passed in separator if one is provided", () => {
		tokenize( "this is a list of tokens", { "separator": "i" } ).should.deep.equal( [ "th", "s ", "s a l", "st of tokens" ] );
	} );

	it( "should not lowercase if that setting is passed in and is false", () => {
		tokenize( "THIS is a LIST of TOKENS", { "lowercase": false } ).should.deep.equal( [ "THIS", "is", "a", "LIST", "of", "TOKENS" ] );
	} );

	it( "should lowercase when a separator is passed in but no lowercase setting is passed in", () => {
		tokenize( "THIS is a LIST of TOKENS", { "separator": "i" } ).should.deep.equal( [ "th", "s ", "s a l", "st of tokens" ] );
	} );

	it( "should split on spaces when a lowercase setting is passed in but no separator is passed in", () => {
		tokenize( "THIS is a LIST of TOKENS", { "lowercase": true } ).should.deep.equal( [ "this", "is", "a", "list", "of", "tokens" ] );
	} );

	it( "should remove leading and trailing spaces from a passed in input string", () => {
		tokenize( "          THIS is a LIST of TOKENS       	    " ).should.deep.equal( [ "this", "is", "a", "list", "of", "tokens" ] );
	} );
} );
