/* global describe it */

import trie from "../js/trie.js";

describe( "trie generator function - trie.js", function TrieSuite(){
	var token = "elephants";
	var nesting = [ "e", "l", "e", "p", "h", "a", "n", "t", "s" ];
	var property = "e.l.e.p.h.a.n.t.s";
	var emptyLeaf = {
		"documents": {},
		"documentFrequency": 0
	};

	it( "should generate deeply nested objects for the token", () => {
		let container = {};

		trie( token, container ).should.deep.equal( emptyLeaf );
		container.should.have.deep.nested.property( property, emptyLeaf );
	} );

	it( "should leave an empty leaf at each step of the token", () => {
		let container = {};

		trie( token, container );

		nesting.forEach( ( prop ) => {
			container.should.have.any.keys( [ prop ] );
			container[ prop ].documents.should.deep.equal( emptyLeaf.documents );
			container[ prop ].documentFrequency.should.deep.equal( emptyLeaf.documentFrequency );

			container = container[ prop ];
		} );
	} );
} );
