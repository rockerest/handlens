/* global describe it */

import settings from "../js/settings.js";

describe( "encapsulated settings module - settings.js", function SettingsSuite(){
	it( "should expose the expected api", () => {
		settings.should.have.all.keys( [
			"get"
		] );
	} );

	describe( "#get", function GetSuite(){
		var settingsClone = settings.get();

		it( "should expose the expected API", () => {
			settingsClone.should.have.all.keys( [
				"documents",
				"tokenize",
				"stopwords"
			] );
		} );

		describe( ".documents", function DocumentsSuite(){
			it( "should have all of the correct default settings", () => {
				settingsClone.documents.should.have.all.keys( [
					"retainAfterIndex"
				] );

				settingsClone.documents.retainAfterIndex.should.equal( true );
			} );
		} );

		describe( ".tokenize", function TokenizeSuite(){
			it( "should have all of the correct default settings", () => {
				settingsClone.tokenize.should.have.all.keys( [
					"separator",
					"lowercase"
				] );

				settingsClone.tokenize.separator.toString().should.equal( "/\\s+/" );
				settingsClone.tokenize.lowercase.should.equal( true );
			} );
		} );

		describe( ".stopwords", function StopwordsSuite(){
			it( "should have all of the correct default settings", () => {
				settingsClone.stopwords.should.have.all.keys( [
					"lang",
					"list"
				] );

				settingsClone.stopwords.lang.should.equal( "en" );
			} );

			describe( ".list", function ListSuite(){
				var langs = [
					"af", "ha", "so", "st", "sw", "yo", "zu", "da",
					"de", "es", "et", "fi", "fr", "hr", "hu", "it",
					"ko", "nl", "no", "pl", "pt", "ru", "sv", "tr",
					"zh", "eo", "he", "la", "sk", "sl", "br", "ca",
					"cs", "el", "eu", "ga", "gl", "hy", "id", "ja",
					"lv", "th", "ar", "bg", "bn", "fa", "hi", "mr",
					"ro", "en"
				];
				/* Is your favorite language not here?
					Add it!
						Do the tests fail now?
							Looks like you're going to be hunting down stopwords for that language!
								You're doing the Lord's work, son. */

				it( "should have every expected language", () => {
					settingsClone.stopwords.list.should.have.all.keys( langs );
				} );

				it( "should have an array of at least one stopword for each language", () => {
					langs.forEach( ( lang ) => {
						settingsClone.stopwords.list[ lang ].should.be.a( "array" );
						( settingsClone.stopwords.list[ lang ].length > 0 ).should.be.ok;
					} );
				} );
			} );
		} );
	} );
} );
