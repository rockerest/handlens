/* global describe it */

import index from "../js/index.js";
import query from "../js/query.js";

describe( "index query function - query.js", function QuerySuite(){
	var andQueries = [
		{
			"bool": "AND",
			"fields": {
				"*": [ "goose" ],
				"body": [ "orinoco" ]
			}
		}
	];
	var orQueries = [
		{
			"bool": "OR",
			"fields": {
				"*": [],
				"body": [ "embden", "brewer's" ]
			}
		}
	];

	it( "should return an empty array if there are no indexed fields", () => {
		var idx = index();

		query( idx, andQueries ).should.deep.equal( [] );
		query( idx, orQueries ).should.deep.equal( [] );
	} );

	it( "should return an empty array if there are no documents", () => {
		var idx = index();

		idx.fields = [ "body", "title" ];
		idx.rebuild();

		query( idx, andQueries ).should.deep.equal( [] );
		query( idx, orQueries ).should.deep.equal( [] );
	} );

	it( "should return an empty array if there are no matching documents", () => {
		var idx = index();

		idx.fields = [ "body", "title" ];
		idx.documents = [
			{
				"id": 1,
				"title": "A Tale of Two Cities",
				"source": "https://en.wikiquote.org/wiki/A_Tale_of_Two_Cities",
				"body": "It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of Light, it was the season of Darkness, it was the spring of hope, it was the winter of despair, we had everything before us, we had nothing before us, we were all going direct to Heaven, we were all going direct the other way – in short, the period was so far like the present period, that some of its noisiest authorities insisted on its being received, for good or for evil, in the superlative degree of comparison only."
			},
			{
				"id": 2,
				"title": "Of Mice And Men",
				"source": "https://2paragraphs.com/2012/08/of-mice-and-men/",
				"body": "A few miles south of Soledad, the Salinas River drops in close to the hillside bank and runs deep and green."
			}
		];
		idx.rebuild();

		query( idx, andQueries ).should.deep.equal( [] );
		query( idx, orQueries ).should.deep.equal( [] );
	} );

	it( "should AND terms in any field properly", () => {
		var idx = index();

		idx.fields = [ "alpha", "beta", "gamma", "delta" ];
		idx.documents = [
			{
				"id": 1,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Embden Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 2,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 3,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Orinoco Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 4,
				"alpha": "Matches Nothing",
				"beta": "Orinoco Embden",
				"gamma": "The Goose CATCH",
				"delta": "The Delta Force"
			}
		];

		idx.rebuild();

		query( idx, [ {
			"bool": "AND",
			"fields": {
				"*": [ "embden", "goose", "catch" ]
			}
		} ] ).should.have.deep.members( [
			{ "ref": "1" },
			{ "ref": "4" }
		] );
	} );

	it( "should AND terms in any field properly - edge case 1", () => {
		var idx = index();

		idx.fields = [ "id", "vehicleDefects" ];
		idx.documents = [
			{
				"id": 1,
				"vehicleDefects": "Battery Belts and Hoses Wipers and Washers Air Compressor Air Lines"
			},
			{
				"id": 2,
				"vehicleDefects": "Battery Belts and Hoses Air Compressor Air Lines Body"
			},
			{
				"id": 3,
				"vehicleDefects": "Air Compressor Air Lines Battery Belts and Hoses Body"
			},
			{
				"id": 4,
				"vehicleDefects": "Air Compressor"
			},
			{
				"id": 5,
				"vehicleDefects": "Battery Air Compressor Air Lines"
			},
			{
				"id": 6,
				"vehicleDefects": "Air Compressor Air Lines Battery"
			},
			{
				"id": 7,
				"vehicleDefects": "Battery Belts and Hoses Body Air Lines"
			},
			{
				"id": 8,
				"vehicleDefects": "Other Air Compressor Air Lines"
			},
			{
				"id": 9,
				"vehicleDefects": "Air Compressor Air Lines Battery"
			}
		];

		idx.rebuild();

		idx.query( [ {
			"bool": "AND",
			"fields": {
				"*": [ "air", "compressor" ]
			}
		} ] ).should.have.deep.members( [
			{ "ref": "1" },
			{ "ref": "2" },
			{ "ref": "3" },
			{ "ref": "4" },
			{ "ref": "5" },
			{ "ref": "6" },
			{ "ref": "8" },
			{ "ref": "9" },
		] );
	} );

	it( "should AND terms in some specific fields properly", () => {
		var idx = index();

		idx.fields = [ "alpha", "beta", "gamma", "delta" ];
		idx.documents = [
			{
				"id": 1,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Embden Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 2,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 3,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Orinoco Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 4,
				"alpha": "Matches Nothing",
				"beta": "Orinoco Embden",
				"gamma": "The Goose CATCH",
				"delta": "The Delta Force"
			}
		];

		idx.rebuild();

		query( idx, [ {
			"bool": "AND",
			"fields": {
				"*": [ "embden", "goose" ],
				"gamma": [ "catch" ]
			}
		} ] ).should.have.deep.members( [
			{ "ref": "1" },
			{ "ref": "4" }
		] );
	} );

	it( "should AND terms in only specific fields properly", () => {
		var idx = index();

		idx.fields = [ "alpha", "beta", "gamma", "delta" ];
		idx.documents = [
			{
				"id": 1,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Embden Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 2,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 3,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Orinoco Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 4,
				"alpha": "Matches Nothing",
				"beta": "Orinoco Embden",
				"gamma": "The Goose CATCH",
				"delta": "The Delta Force"
			}
		];

		idx.rebuild();

		query( idx, [ {
			"bool": "AND",
			"fields": {
				"*": [],
				"beta": [ "embden", "goose" ],
				"gamma": [ "catch" ]
			}
		} ] ).should.have.deep.members( [
			{ "ref": "1" },
			{ "ref": "4" }
		] );
	} );

	it( "should OR terms in any field properly", () => {
		var idx = index();

		idx.fields = [ "alpha", "beta", "gamma", "delta" ];
		idx.documents = [
			{
				"id": 1,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Embden Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 2,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 3,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Orinoco Goose",
				"gamma": "The Gamma Field",
				"delta": "The Beta Force"
			},
			{
				"id": 4,
				"alpha": "Matches Nothing",
				"beta": "Orinoco Embden",
				"gamma": "The Goose CATCH",
				"delta": "The Beta Force"
			}
		];

		idx.rebuild();

		query( idx, [ {
			"bool": "OR",
			"fields": {
				"*": [ "embden", "delta" ]
			}
		} ] ).should.have.deep.members( [
			{ "ref": "1" },
			{ "ref": "2" },
			{ "ref": "4" }
		] );
	} );

	it( "should OR terms in some specific fields properly", () => {
		var idx = index();

		idx.fields = [ "alpha", "beta", "gamma", "delta" ];
		idx.documents = [
			{
				"id": 1,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Embden Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 2,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose",
				"gamma": "The Gamma Field",
				"delta": "The Embden Force"
			},
			{
				"id": 3,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Orinoco Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 4,
				"alpha": "Matches Nothing",
				"beta": "Orinoco Embden",
				"gamma": "The Goose CATCH",
				"delta": "The Delta Force"
			}
		];

		idx.rebuild();

		query( idx, [ {
			"bool": "OR",
			"fields": {
				"*": [ "embden" ],
				"gamma": [ "catch" ]
			}
		} ] ).should.have.deep.members( [
			{ "ref": "1" },
			{ "ref": "2" },
			{ "ref": "3" },
			{ "ref": "4" }
		] );
	} );

	it( "should OR terms in only specific fields properly", () => {
		var idx = index();

		idx.fields = [ "alpha", "beta", "gamma", "delta" ];
		idx.documents = [
			{
				"id": 1,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Embden Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 2,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			},
			{
				"id": 3,
				"alpha": "Matches Nothing",
				"beta": "Silly Goose Orinoco Goose",
				"gamma": "The Gamma CATCH",
				"delta": "The Delta Force"
			},
			{
				"id": 4,
				"alpha": "Matches Nothing",
				"beta": "Orinoco Embden",
				"gamma": "The Gamma Field",
				"delta": "The Delta Force"
			}
		];

		idx.rebuild();

		query( idx, [ {
			"bool": "OR",
			"fields": {
				"*": [],
				"beta": [ "embden" ],
				"gamma": [ "catch" ]
			}
		} ] ).should.have.deep.members( [
			{ "ref": "1" },
			{ "ref": "3" },
			{ "ref": "4" }
		] );
	} );
} );
