/* global describe it sinon before beforeEach */

import tokenize from "../js/tokenize.js";
import trim from "../js/trim.js";
import settings from "../js/settings.js";

import index from "../js/index.js";
import query from "../js/query.js";

describe( "handlens exposed index API function - index.js", function IndexApiSuite(){
	it( "should expose the correct API", () => {
		index().should.have.all.keys( [
			"settings",
			"fields",
			"ref",
			"documents",
			"pipeline",
			"index",
			"addDocument",
			"addField",
			"rebuild",
			"search",
			"query"
		] );
	} );

	it( "should return an empty, usable index in the default state", () => {
		let idx = index();

		idx.should.have.deep.property( "settings", settings.get() );
		idx.fields.should.be.a( "array" );
		idx.fields.length.should.equal( 0 );
		idx.ref.should.equal( "id" );
		idx.documents.should.be.a( "array" );
		idx.documents.length.should.equal( 0 );
		idx.pipeline.should.be.a( "array" );
		idx.pipeline.length.should.equal( 3 );
		idx.index.should.be.a( "object" );
		Object.keys( idx.index ).length.should.equal( 0 );
	} );

	it( "should return a usable default pipeline", () => {
		let idx = index();

		idx.pipeline[ 0 ].should.equal( tokenize );
		idx.pipeline[ 1 ].should.equal( trim );
		idx.pipeline[ 2 ].should.be.a( "function" );
		idx.pipeline[ 2 ].toString().should.equal( "( tokens ) => stopwords( tokens, idx.settings.stopwords.list[ idx.settings.stopwords.lang ] )" ); // two identical functions do not compare as equivalent
	} );

	describe( "#addDocument", function AddDocumentSuite(){
		it( "should index a new document", () => {
			let idx = index();

			Object.keys( idx.index ).length.should.equal( 0 );

			idx.fields = [ "alpha" ];
			idx.rebuild();

			idx.addDocument( {
				"id": 1,
				"alpha": "a test field"
			} );

			Object.keys( idx.index.alpha ).length.should.equal( 4 ); // "T"est "F"ield + "documents" and "documentFrequency" ("a" is stripped as a stopword)
			idx.index.alpha.t.e.s.t.documents.should.have.all.keys( [ "1" ] );
		} );

		it( "should add the new document to the list of documents if documents are set to be retained", () => {
			let idx = index();

			idx.documents.length.should.equal( 0 );

			idx.fields = [ "alpha" ];
			idx.rebuild();

			idx.addDocument( {
				"id": 1,
				"alpha": "a test field"
			} );

			idx.documents.length.should.equal( 1 );
		} );

		it( "should not add the new document to the list of documents if documents are not set to be retained", () => {
			let idx = index();

			idx.documents.length.should.equal( 0 );

			idx.settings.documents.retainAfterIndex = false;
			idx.fields = [ "alpha" ];
			idx.rebuild();

			idx.addDocument( {
				"id": 1,
				"alpha": "a test field"
			} );

			idx.documents.length.should.equal( 0 );
		} );
	} );

	describe( "#addField", function AddFieldSuite(){
		it( "should add the new field to the stored list of fields", () => {
			let idx = index();

			idx.fields.should.deep.equal( [] );

			idx.addField( "alpha" );

			idx.fields.should.deep.equal( [ "alpha" ] );
		} );

		it( "should add the new field to the index", () => {
			let idx = index();

			Object.keys( idx.index ).length.should.equal( 0 );

			idx.addField( "alpha" );

			idx.index.should.have.property( "alpha" );
		} );

		it( "should rebuild the index since the fields changed", () => {
			let idx = index();
			let rebuildStub = sinon.stub( idx, "rebuild" );

			idx.addField( "alpha" );

			rebuildStub.should.have.been.calledOnce;
		} );
	} );

	describe( "#rebuild", function RebuildSuite(){
		it( "should create a new search index", () => {
			let idx = index();

			idx.fields.length.should.equal( 0 );
			Object.keys( idx.index ).length.should.equal( 0 );

			idx.fields = [ "alpha", "beta" ];

			idx.rebuild();

			Object.keys( idx.index ).length.should.equal( 2 );
		} );

		it( "should remove documents after generating the new search index if the retainAfterIndex setting is false", () => {
			let idx = index();

			idx.fields = [ "alpha" ];
			idx.documents = [ {
				"id": 1,
				"alpha": "alpha"
			} ];
			idx.settings.documents.retainAfterIndex = false;

			idx.rebuild();

			idx.index.should.have.all.keys( "alpha" );
			idx.index.should.have.nested.property( "alpha.a.l.p.h.a" );
			idx.documents.length.should.equal( 0 );
		} );
	} );

	describe( "#search", function SearchSuite(){
		var idx;
		var queryStub;

		before( () => {
			idx = index();

			idx.fields = [ "alpha" ];
			idx.documents = [
				{
					"id": 1,
					"alpha": "canadian goose"
				},
				{
					"id": 2,
					"alpha": "orinoco goose"
				},
				{
					"id": 3,
					"alpha": "cotton patch goose"
				}
			];

			idx.rebuild();

			queryStub = sinon.stub( idx, "query" );
		} );

		beforeEach( () => {
			queryStub.resetHistory();
		} );

		it( "should call the query function once", () => {
			idx.search( "test" );

			queryStub.should.have.been.calledOnce;
		} );

		it( "should call the query function with the appropriate queries", () => {
			let queries = [
				{
					"bool": "OR",
					"fields": {
						"*": [ "test" ]
					}
				}
			];

			idx.search( "test" );

			queryStub.should.have.been.calledWithExactly( queries );
		} );

		it( "should call the query function with an empty array if the query is not parseable", () => {
			let queries = [];

			idx.search( "" );
			idx.search( undefined );
			idx.search( null );
			idx.search( true );
			idx.search( 1 );
			idx.search( /\s+/ );
			idx.search( new Set() );
			idx.search( new Map() );

			queryStub.should.have.been.calledWithExactly( queries );
		} );
	} );

	describe( "#query", function QuerySuite(){
		it( "should return the results of calling the query function with the index as the first parameters and the passed-in queries as the second", () => {
			let idx = index();
			let queries = [
				{
					"bool": "OR",
					"fields": {
						"*": [ "test" ]
					}
				}
			];

			idx.query( queries ).should.deep.equal( query( idx, queries ) );
		} );
	} );
} );
