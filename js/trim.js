function trim( tokens ){
	return tokens.map(
		( token ) =>
			token
				.replace( /([!&*()+=_~`|\\{}[\];"<>,.?/]|'$|^')/gm, "" )
				.trim()
	);
}

export default trim;
