/* global describe it */

import index from "../js/index.js";
import querybuilder from "../js/querybuilder.js";

describe( "string -> query builder function - querybuilder.js", function QuerybuilderSuite(){
	var idx = index();

	idx.fields = [ "alpha", "beta" ];
	idx.rebuild();

	it( "should return an empty array for non-strings", () => {
		querybuilder( undefined, idx ).should.deep.equal( [] );
		querybuilder( null, idx ).should.deep.equal( [] );
		querybuilder( 1, idx ).should.deep.equal( [] );
		querybuilder( true, idx ).should.deep.equal( [] );
		querybuilder( /\s+/, idx ).should.deep.equal( [] );
		querybuilder( {}, idx ).should.deep.equal( [] );
		querybuilder( [], idx ).should.deep.equal( [] );
		querybuilder( new Set(), idx ).should.deep.equal( [] );
		querybuilder( new Map(), idx ).should.deep.equal( [] );
		querybuilder( Symbol(), idx ).should.deep.equal( [] );
	} );

	it( "should return an empty array for only booleans", () => {
		querybuilder( "AND OR AND AND AND AND OR OR OR AND AND OR", idx ).should.deep.equal( [] );
	} );

	it( "should return an empty array for only booleans and/or stopwords", () => {
		querybuilder( "AND OR AND there's AND hello there AND AND OR OR OR AND AND OR", idx ).should.deep.equal( [] );
		idx.settings.stopwords.lang = "de";
		querybuilder( "möglich", idx ).should.deep.equal( [] );
		idx.settings.stopwords.lang = "ko";
		querybuilder( "고려하면", idx ).should.deep.equal( [] );
		idx.settings.stopwords.lang = "en";
	} );

	it( "should return the correct set of queries for a usable string input", () => {
		let queries = querybuilder( "AND AND AND helicopter hello OR alpha:tom AND harry alpha:dick AND harry gamma:friends beta:stuff AND garbage AND mammal beta:state AND garbage AND mammal AND AND     ", idx );

		queries.length.should.equal( 6 );
		queries.should.deep.equal( [
			{
				"bool": "OR",
				"fields": {
					"*": [ "helicopter" ]
				}
			}, // hello is stripped because it's an English stopword
			{
				"bool": "AND",
				"fields": {
					"*": [ "harry" ],
					"alpha": [ "tom" ]
				}
			},
			{
				"bool": "AND",
				"fields": {
					"*": [ "harry" ],
					"alpha": [ "dick" ]
				}
			},
			{
				"bool": "OR",
				"fields": {
					"*": [ "gamma:friends" ] // gamma isn't extracted as a field because that field isn't listed as an indexed field
				}
			},
			{
				"bool": "AND",
				"fields": {
					"*": [ "garbage", "mammal" ],
					"beta": [ "stuff" ]
				}
			},
			{
				"bool": "AND",
				"fields": {
					"*": [ "garbage", "mammal" ],
					"beta": [ "state" ]
				}
			}
		] );
	} );
} );
