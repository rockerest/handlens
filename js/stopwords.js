function stopwords( tokens, keyedStopWords ){
	var words = [];

	for( let lang in keyedStopWords ){
		words = words.concat( keyedStopWords[ lang ] );
	}

	return tokens.filter( ( token ) => {
		let keep = Boolean( token );

		if( words.includes( token ) ){
			keep = false;
		}

		return keep;
	} );
}

export default stopwords;
